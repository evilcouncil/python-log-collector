python-log-collector
---
Pulls data from [log-serve](https://gitlab.com/evilcouncil/log-serve).

Example Code
```
from python-log-collector import logcollector

collector = logcollector.LogCollector("log-serve.example.com", "8080", "client.crt", "client.key", "ca_cert.pem")

files = collector.list_files()
for file in files:
    print(collector.get_file(file))
    collector.delete_file(file)
```
